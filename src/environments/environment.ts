// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/**
 * @Return pokeApi = https://pokeapi.co/api/v2/pokemon
 * @Return trainerUrl = https://as-api-lot.herokuapp.com/trainers
 */
export const environment = {
  production: false,
  trainerUrl: 'https://as-api-lot.herokuapp.com/trainers',
  pokeApi: 'https://pokeapi.co/api/v2/pokemon',
  apiKey: 'ey9sfF@MgnoXD(^9UbC-RPro5-kU6uXyX_UVdDs^',
  apiUrl: "https://as-api-lot.herokuapp.com/trainers"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
