import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  constructor(private readonly router: Router ) { }

  ngOnInit(): void {  
  }

  onLogin(logged:boolean):void{
    if(logged === true) this.router.navigateByUrl("/catalogue");
  }
}
