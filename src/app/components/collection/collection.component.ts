import { Component, OnInit} from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { StorageUtil } from 'src/app/utils/storage.util';
import { StorageKeys } from 'src/app/utils/storage-keys.enum';
import { User } from 'src/app/models/user.model';
import { CollectionService } from 'src/app/services/collection.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {
  private trainer: User = StorageUtil.storageRead(StorageKeys.User)!;
  private pokemons: Pokemon[] = [];
  private pokemonsPage: Pokemon[] = [];  
  private beginPageAtId: number = 0;
  private endPageAtId: number = 20;

  constructor(private collectionService: CollectionService) { }

  get fromPokemonId():number{
    return this.beginPageAtId;
  }
  get toPokemonId():number{
    if(this.endPageAtId>this.pokemons.length) return this.pokemons.length;
    return this.endPageAtId;
  }
  get pokePage():Pokemon[]{
    return this.pokemonsPage;
  }
  get amountOfPokemons():number{    
    return this.pokemons.length;
  }

  ngOnInit(): void {    
     this.getPokemonsInCollection();
  }  
  updateCollection(pokemon:Pokemon):void{
    this.trainer.pokemon = this.trainer.pokemon.filter((resp:string) => resp !== pokemon.name);
    this.collectionService.updateCollection(this.trainer).subscribe();
    this.getPokemonsInCollection();
  }
  // Show previous 20 pokemons
  onPreviousClick():void{
    if(this.beginPageAtId !== 0){
      this.beginPageAtId -= 20;
      this.endPageAtId -= 20;
      this.pokemonsPage = this.pokemons.slice(this.beginPageAtId,this.endPageAtId); 
    }        
  }
  // Show next 20 pokemons
  onNextClick():void{
    this.beginPageAtId += 20;
    this.endPageAtId += 20;
    this.pokemonsPage = this.pokemons.slice(this.beginPageAtId,this.endPageAtId);
  }
  // Get all the pokemons in current users collection
  getPokemonsInCollection(){
    this.pokemons = StorageUtil.storageRead<Pokemon[]>(StorageKeys.Pokemons)!
      .filter((p) => this.trainer.pokemon.includes(p.name!))
    this.pokemonsPage = this.pokemons.slice(this.beginPageAtId,this.endPageAtId);
  }
}