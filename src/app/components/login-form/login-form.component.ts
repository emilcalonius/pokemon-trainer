import { Component, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { NgForm } from '@angular/forms';
import * as bcrypt from 'bcryptjs';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<boolean> = new EventEmitter();

  showPasswordInput = false;
  userExists = false;
  username = "";
  showWarning = false;
  warningMessage = "";

  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService
  ) { }

  /**
   * Runs after submitting username. 
   * Check if given username already exists in "database".  
   * 
   * @param usernameForm 
   * @returns void
   */
  public usernameSubmit(usernameForm: NgForm): void {
    if (usernameForm.value.username === "") return;
    this.username = usernameForm.value.username;    
    this.loginService.checkUsername(this.username).subscribe((user: User | undefined) => {
      user === undefined ? this.userExists = false : this.userExists = true
      this.showPasswordInput = true;
    });
  }

  /**
   * Runs after submitting password.
   * Log user in.
   * 
   * @param loginForm 
   * @returns void
   */
  public loginSubmit(loginForm: NgForm): void {
    
    const { passwordOne, passwordTwo } = loginForm.value;

    // Don't accept empty passwords
    if (passwordOne === "" || passwordTwo === "") return; 

    let newPassword = "";
    // If user doesn't exist and given passwords match -> encrypt password
    if (!this.userExists && passwordOne === passwordTwo) {
      const salt = bcrypt.genSaltSync(10);
      newPassword = bcrypt.hashSync(passwordOne, salt);
    } 
    // If user doesn't exists but given passwords do not match -> display warning for user and return
    if (!this.userExists && passwordOne !== passwordTwo) {
      this.showWarning = true;
      this.warningMessage = "Passwords do not match!";
      return;
    }

    this.loginService.login(this.username, newPassword)
      .subscribe({
        next: (user: User | undefined) => {
          if(user !== undefined) {
            if(bcrypt.compareSync(passwordOne, user.password)) {
              this.showWarning = false;
              this.userService.user = user;
              this.login.emit(true);
            } else {
              this.showWarning = true;
              this.warningMessage = "Wrong password!";
            }
          }
        },
        error: () => {

        }
      })
  }
}
