import { Component, OnDestroy, OnInit} from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Subscription } from 'rxjs';
import { CollectionService } from 'src/app/services/collection.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import { StorageKeys } from 'src/app/utils/storage-keys.enum';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  styleUrls: ['./catalogue-list.component.scss']
})
export class CatalogueListComponent implements OnInit, OnDestroy {
  private trainer: User = StorageUtil.storageRead(StorageKeys.User)!;
  private pokemons$: Subscription;
  private pokemons: Pokemon[] = [];
  private pokemonsPage: Pokemon[] = [];  
  private beginPageAtId: number = 0;
  private endPageAtId: number = 20;

  constructor(
    private pokemonService:PokemonService, 
    private collectionService:CollectionService) {
      this.pokemons$ = this.pokemonService.pokemons$.subscribe((pokemons: Pokemon[]) => {
        this.setPokes(pokemons);              
      })
  }

  get fromPokemonId():number{
    return this.beginPageAtId;
  }
  get toPokemonId():number{
    return this.endPageAtId;
  }
  get pokePage():Pokemon[]{
    return this.pokemonsPage;
  }
  get amountOfPokemons():number{
    return this.pokemons.length;
  }  
  // Update trainers collection (remove or add a pokemon)
  updateCollection(pokemon:Pokemon):void{
    if(pokemon.name){
      if(this.trainer.pokemon.includes(pokemon.name)){
        this.trainer.pokemon = this.trainer.pokemon.filter((resp:string) => resp !== pokemon.name);
      }else{
        this.trainer.pokemon = [...this.trainer.pokemon,pokemon.name]
      }      
      this.collectionService.updateCollection(this.trainer).subscribe();
    }    
  }
  // Is pokemon in current users collection
  inCollection(pokemon:string):boolean{
    return this.trainer.pokemon.includes(pokemon);    
  }
  // Set pokemons for viewing
  setPokes(pokes:Pokemon[]):void{
    this.pokemons = pokes;    
    this.pokemonsPage = pokes.slice(this.beginPageAtId,this.endPageAtId);
  }
  // Show previous 20 pokemons
  onPreviousClick():void{
    if(this.beginPageAtId !== 0){
      this.beginPageAtId -= 20;
      this.endPageAtId -= 20;
      this.pokemonsPage = this.pokemons.slice(this.beginPageAtId,this.endPageAtId); 
    }        
  }
  // Show next 20 pokemons
  onNextClick():void{
    this.beginPageAtId += 20;
    this.endPageAtId += 20;
    this.pokemonsPage = this.pokemons.slice(this.beginPageAtId,this.endPageAtId);
  }
  // Call pokemonService to fetch pokemons from API or if they allready exist in sessionStorage
  // use it instead
  ngOnInit(): void {
    const inSessionPokemons = StorageUtil.storageRead<Pokemon[]>(StorageKeys.Pokemons);
    if(inSessionPokemons !== undefined && inSessionPokemons!.length>1  ){
      this.setPokes(inSessionPokemons)     
    }else{
      this.pokemonService.getPokemon();
    }      
  }
  ngOnDestroy(): void {
    this.pokemons$.unsubscribe();
  }
}
