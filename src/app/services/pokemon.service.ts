import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
import { map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { StorageKeys } from '../utils/storage-keys.enum';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  public readonly pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject<Pokemon[]>([]);
  public offset: number = 0;
  public limit: number = 1200;

  constructor(private readonly http: HttpClient) { }

  /**
   * Get full list of pokemons from pokeAPI, if they dont allready reside in session storage
   */
  getPokemon(): void{
    const inSessionPokemons = StorageUtil.storageRead<Pokemon[]>(StorageKeys.Pokemons);
      if(inSessionPokemons !== undefined && inSessionPokemons!.length>1  ){
      return;     
    }
    this.http.get<Pokemon[]>(`${environment.pokeApi}/?offset=${this.offset}&limit=${this.limit}`)
    .pipe(
      map((response: any) => response.results.map( (pokemon:any) => {
          return {
            ...pokemon,
            ...this.getPokemonImageAndId(pokemon.url)
          };
        }
      ))
    ).toPromise().then((pokemons: Pokemon[]) => {
      this.pokemons$.next(pokemons);
      StorageUtil.storageSave(StorageKeys.Pokemons,pokemons);
      });    
  } 
  /**
   * Get pokemons id and image
   * @param url 
   * @returns object containing id and url to pokemons image
   */
  getPokemonImageAndId(url:string):any{
    let id:string | undefined;
    if(url.includes(".png")) id = url.replace(".png", "").split('/').filter(Number).pop();
    else id = url.split('/').filter(Number).pop();
    
    return {id, url:`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
  }
}
