import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { UserService } from './user.service';
import { StorageKeys } from '../utils/storage-keys.enum';

const { apiUrl, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private readonly http: HttpClient, 
    private readonly userService: UserService,
    private router: Router
  ) { }

  /**
   * Get a user from the "database" or create a new user if given username does not exist.
   * 
   * @param username 
   * @param password encrypted password
   * @returns Observable of the user object
   */
  public login(username: string, password: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
        if (user === undefined) {
          return this.createUser(username, password);
        }
        return of(user);
      }))
    }

  public checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUrl}?username=${username}`)
      .pipe(map((response: User[]) => response.pop()));
  }

  private createUser(username: string, password: string): Observable<User> {
    const user = {
      username,
      password,
      pokemon: []
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    return this.http.post<User>(apiUrl, user, { 
      headers 
    });
  }

  public logout(): void {
    this.router.navigateByUrl("/");
    sessionStorage.removeItem(StorageKeys.User);
    this.userService.user = undefined;
  }

  public isLoggedIn(): boolean {
    return StorageUtil.storageRead("user") !== undefined;
  }
}
