import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from '../utils/storage-keys.enum';
import { User } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class CollectionService {
  private httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'x-api-key': environment.apiKey })
  };
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log((`${operation} failed: ${error.message}`));
      return of(result as T);
    };
  }

  constructor(private readonly http: HttpClient) { }  
  
  /**
   * PATCH - updates trainers pokemons
   * 
   * @param trainer 
   * @returns 
   */
  updateCollection(trainer: User): Observable<string> {    
    return this.http.patch<string>(`${environment.trainerUrl}/${trainer.id}`,
    {pokemon: trainer.pokemon},this.httpOptions)
      .pipe(
        tap(()=> StorageUtil.storageSave<User>(StorageKeys.User,trainer)),
        catchError(this.handleError<string>('updateCollection'))      
      );  
  } 
}
