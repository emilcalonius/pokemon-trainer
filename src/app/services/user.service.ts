import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

  constructor() {
    this._user = StorageUtil.storageRead<User>("user");
  }

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    console.log(user);
    if(user === undefined) {
      this._user = user;
      return;
    }
    // Add user information to session storage excluding password
    const newUser = { 
      username: user!.username, 
      id: user!.id,
      pokemon: user!.pokemon
    };
    StorageUtil.storageSave<Object>("user", newUser);
    this._user = user;
  }
}
