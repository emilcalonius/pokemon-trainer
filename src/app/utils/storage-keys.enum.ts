export enum StorageKeys {
    Trainer = "trainer",
    Pokemons = "pokemons",
    User = "user"  
}