export class StorageUtil {
    /**
     * Save a key value pair to session storage.
     * 
     * @param key 
     * @param value 
     */
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    /**
     * Read a value from session storage.
     * 
     * @param key 
     * @returns value stored in session storage or undefined
     */
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try {
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            }
            return undefined;
        } catch(e) {
            sessionStorage.removeItem(key);
            return undefined;
        }
    }
}