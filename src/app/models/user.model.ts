import { Pokemon } from "./pokemon.model";

export interface User {
    id: number;
    username: string;
    password: string;
    pokemon: string[];
    //pokemon: Pokemon[];
}