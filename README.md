
# Pokémon Trainer
## Table of contents
* [Introduction](#introduction)
* [Features](#features)
* [Technologies](#technologies)
* [Usage](#usage)
* [Authors](#authors)
* [Sources](#sources)

## Introduction
Single page application for managing personal collection of Pokémon. The application has three pages: login, catalogue and profile.
Login page is for logging in to the application or creating a new user. In catalogue page the user can view a list of all Pokémon from PokéAPI 
and add Pokémon to their personal collection. Profile page displays the user's collection of Pokémon.

## Features
- Authentication / authorization
- Display list of all pokemon from [PokéAPI](https://pokeapi.co/)
- Add/remove Pokémon to/from user's personal collection

## Technologies
- TypeScript
- Angular
- HTML/CSS
- Render

## Usage
App deployed on Render and can be accessed [here](https://poke-trainer.onrender.com/login)

## Authors
[@Emil](https://gitlab.com/emilcalonius)<br />
[@Jani](https://gitlab.com/janijk)

## Sources
Project was an assignment done during education program created by
Noroff Education